from pathlib import Path
from typing import Dict, Type, Any
from io import BytesIO
import pytest
from pytest import MonkeyPatch
from pytest_subprocess import FakeProcess
from pytest_subprocess.fake_popen import FakePopen
from matchms.exporting import save_as_mgf
from cfm_id import CfmId, CfmIdDocker, CfmIdTCP
from cfm_id.cfm_id import CfmIdBase

CFMID_PATH = "CFMID_PATH"
CFMID_IMAGE = "CFMID_IMAGE"
CFMID_HOST = "CFMID_HOST"
CFMID_PORT = "CFMID_PORT"

smiles_to_inchi_key = {
    "O=C1OC(CO)C(O)=C1O": "ZZZCUOFIHGPKAK-UHFFFAOYSA-N",
    "CC(NC(CCCCN)C(=O)O)C(=O)O": "ZZYYVZYAZCMNPG-RQJHMYQMSA-N",
}


@pytest.fixture
def envs_dict(shared_datadir: Path) -> Dict[str, Any]:
    return {
        CFMID_PATH: str(shared_datadir / "cfm_id"),
        CFMID_IMAGE: "cfmid/cfmid:latest",
        CFMID_HOST: "cfmid",
        CFMID_PORT: "7777",
    }


@pytest.fixture
def cfmid_envs(envs_dict: Dict[str, str], monkeypatch: MonkeyPatch) -> Dict[str, str]:

    for key, value in envs_dict.items():
        monkeypatch.setenv(key, value)
    return envs_dict


@pytest.fixture(autouse=True)
def mock_cfmid(
    envs_dict: Dict[str, str], fake_process: FakeProcess, shared_datadir: Path
) -> None:
    def callback_stdout(process: FakePopen, arg_pos: int) -> None:
        smiles = str(process.args[arg_pos])
        inchikey = smiles_to_inchi_key[smiles]
        if str(process.args[-2]).endswith("param_config_neg.txt"):
            inchikey += "-neg"
        response = (shared_datadir / f"{inchikey}.cfmid").read_bytes()
        process.stdout = BytesIO(response)

    def local_callback(process: FakePopen) -> None:
        callback_stdout(process, 1)

    fake_process.register_subprocess(
        [f"{envs_dict[CFMID_PATH]}/cfm-predict", fake_process.any()],
        callback=local_callback,
    )

    def docker_callback(process: FakePopen) -> None:
        callback_stdout(process, 4)

    fake_process.register_subprocess(
        [
            "docker",
            "run",
            envs_dict[CFMID_IMAGE],
            "cfm-predict",
            fake_process.any(),
        ],
        callback=docker_callback,
    )

    smiles = ""

    def echo_callback(process: FakePopen) -> None:
        nonlocal smiles
        smiles = str(process.args[2])

    fake_process.register_subprocess(
        [
            "echo",
            "cfm-predict",
            fake_process.any(),
        ],
        callback=echo_callback,
    )

    def netcat_callback(process: FakePopen) -> None:
        nonlocal smiles
        inchikey = smiles_to_inchi_key[smiles]
        if str(process.args[-2]).endswith("param_config_neg.txt"):
            inchikey += "-neg"
        response = (shared_datadir / f"{inchikey}.cfmid").read_bytes()
        process.stdout = BytesIO(response)

    fake_process.register_subprocess(
        [
            "netcat",
            f"{envs_dict[CFMID_HOST]}",
            envs_dict[CFMID_PORT],
        ],
        callback=netcat_callback,
    )
    fake_process.keep_last_process(True)


@pytest.fixture(autouse=True)
def mock_ls(
    envs_dict: Dict[str, str], fake_process: FakeProcess, shared_datadir: Path
) -> None:
    def docker_callback(process: FakePopen) -> None:

        file_path = shared_datadir / "cfm_id" / str(process.args[-1])
        process.stdout = BytesIO(file_path.read_bytes())

    fake_process.register_subprocess(
        [
            "docker",
            "run",
            f"{envs_dict[CFMID_IMAGE]}",
            "cat",
            fake_process.any(),
        ],
        callback=docker_callback,
    )
    fake_process.keep_last_process(True)


@pytest.mark.parametrize(
    "cfmid_class, env_key",
    ((CfmId, CFMID_PATH), (CfmIdDocker, CFMID_IMAGE), (CfmIdTCP, CFMID_HOST)),
)
@pytest.mark.parametrize("with_env", (True, False))
def test_predict_single(
    shared_datadir: Path,
    cfmid_class: Type[CfmIdBase],
    env_key: str,
    with_env: bool,
    cfmid_envs: Dict[str, str],
) -> None:

    if cfmid_class == CfmIdTCP:
        kwargs = {"params_dir": shared_datadir / "cfm_id"}
    else:
        kwargs = {}

    if with_env:
        cfm_id = cfmid_class(**kwargs)
    else:
        env_value = cfmid_envs[env_key]
        cfm_id = cfmid_class(env_value, **kwargs)

    smiles = list(smiles_to_inchi_key)[0]

    inchi_key = smiles_to_inchi_key[smiles]
    expected_raw_text = (shared_datadir / f"{inchi_key}.cfmid").read_text()
    raw_text = cfm_id.predict(smiles, include_annotations=False, raw_format=True)
    assert raw_text == expected_raw_text
    spectra = cfm_id.predict(smiles)
    mgf_path = shared_datadir / f"{inchi_key}.mgf"
    mgf_expected_data = mgf_path.read_text()
    mgf_path.unlink()
    save_as_mgf(spectra, str(mgf_path))
    assert mgf_path.read_text() == mgf_expected_data


def test_predict_many(
    shared_datadir: Path,
    cfmid_envs: Dict[str, str],
) -> None:
    smiles = list(smiles_to_inchi_key)
    cfm_id = CfmId()
    spectra = cfm_id.predict(smiles)
    mgf_path = shared_datadir / "many.mgf"
    mgf_expected_data = mgf_path.read_text()
    mgf_path.unlink()
    save_as_mgf(spectra, str(mgf_path))
    assert mgf_path.read_text() == mgf_expected_data
    with pytest.raises(AttributeError):
        cfm_id.predict(smiles, raw_format=True)


@pytest.mark.parametrize(
    "cfmid_class, env_key", ((CfmId, CFMID_PATH), (CfmIdDocker, CFMID_IMAGE))
)
def test_predict_params(
    shared_datadir: Path,
    cfmid_class: Type[CfmIdBase],
    env_key: str,
    cfmid_envs: Dict[str, str],
) -> None:

    cfm_id = cfmid_class(
        param=["param", "param_output0_neg.log"], conf=["conf", "param_config_neg.txt"]
    )

    smiles = list(smiles_to_inchi_key)[0]

    inchi_key = smiles_to_inchi_key[smiles]

    spectra = cfm_id.predict(smiles)
    mgf_path = shared_datadir / f"{inchi_key}-neg.mgf"
    mgf_expected_data = mgf_path.read_text()
    mgf_path.unlink()
    save_as_mgf(spectra, str(mgf_path))
    assert mgf_path.read_text() == mgf_expected_data


def test_predict_error(
    cfmid_envs: Dict[str, str],
) -> None:
    cfm_id = CfmId()
    with pytest.raises(AttributeError):
        cfm_id.predict(123)  # type: ignore


def test_cfm_id_cmd_error(monkeypatch: MonkeyPatch) -> None:
    monkeypatch.delenv(CFMID_PATH, raising=False)
    with pytest.raises(AttributeError):
        CfmId()
