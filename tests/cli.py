import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8 cfm_id"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict cfm_id"
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = "pytest --cov-report term-missing --cov=cfm_id --disable-warnings"
    subprocess.run(shlex.split(cmd))
